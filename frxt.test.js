const frxt = require('./dist')

test('banana', () => {
  expect('banana').toBe('banana')
})


test('Group Conjunctive Normal Forms', () => {
  let filters = [
    {logic: 'and'},
    {logic: 'and'},
    {logic: 'and'},
  ]
  let grouped = frxt.groupByConjunctiveNormalForm(filters)
  expect(grouped.length).toBe(3)

  filters = [
    {logic: 'and'},
    {logic: 'or'},
    {logic: 'and'},
  ]
  grouped = frxt.groupByConjunctiveNormalForm(filters)
  expect(grouped.length).toBe(2)

  filters = [
    {logic: 'and'},
    {logic: 'or'},
    {logic: 'and'},
    {logic: 'and'},
    {logic: 'or'},
    {logic: 'or'},
    {logic: 'and'},
  ]
  grouped = frxt.groupByConjunctiveNormalForm(filters)
  expect(grouped.length).toBe(4)
})


test('Apples or Bananas', () => {
  let json = {
    'banana': {
      'name': 'banana'
    },
    'pear': {
      'name': 'pear'
    },
    'apple': {
      'name': 'apple'
    }
  }

  let filters = [
    {
      logic: 'and',
      function: 'identity',
      field: 'name',
      conditional: 'ss',
      input: 'ana'
    },
    {
      logic: 'or',
      function: 'identity',
      field: 'name',
      conditional: 'ss',
      input: 'ppl'
    },
  ]

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(2)
  expect(res[0]).toBe('banana')
  expect(res[1]).toBe('apple')
})


test('fruit is banana or type contains app', () => {
  let inputString = 'fruit is banana or type contains app'
  let tknFieldMap = {
    'fruit': 'fruit',
    'Type': 'fruit',
    'name': 'fruit',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern:frxt.configs.PunctuationPattern,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(2)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('fruit')
  expect(valid[1].filter.field).toBe('fruit')
})

test('filter for "fruit is banana or type contains app"', () => {
  let json = {
    'banana': {
      'fruit': 'banana'
    },
    'pear': {
      'fruit': 'pear'
    },
    'apple': {
      'fruit': 'apple'
    }
  }

  let inputString = 'fruit is banana or type contains app'
  let tknFieldMap = {
    'fruit': 'fruit',
    'Type': 'fruit',
    'name': 'fruit',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern:frxt.configs.PunctuationPattern,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(2)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('fruit')
  expect(valid[1].filter.field).toBe('fruit')

  let filters = valid.map(({filter})=>filter)

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(2)
  expect(res[0]).toBe('banana')
  expect(res[1]).toBe('apple')
})


test('filter for " fruit is "banana or apple" "', () => {
  let json = {
    'banana': {
      'fruit': 'banana'
    },
    'banana-or-apple': {
      'fruit': 'banana or apple'
    },
    'apple': {
      'fruit': 'apple'
    }
  }

  let inputString = 'fruit is "banana or apple"'
  let tknFieldMap = {
    'fruit': 'fruit',
    'Type': 'fruit',
    'name': 'fruit',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern: /(\w+(?:[.,-]\w+)*)|[-.,()&$#![\]{}"']+/g,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(1)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('fruit')

  let filters = valid.map(({filter})=>filter)

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(1)
  expect(res[0]).toBe('banana-or-apple')
})

test('catch negatives', () => {
  let json = {
    '-1': {
      'samples': '1'
    },
  }

  let inputString = 'samples gt -1'
  let tknFieldMap = {
    'samples': 'samples',
    'Type': 'samples',
    'name': 'samples',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern: /(\w+(?:[".,-]\w+)*)|[.,()&$#![\]{}']+/g,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(1)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('samples')

  let filters = valid.map(({filter})=>filter)

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(1)
  expect(res[0]).toBe('-1')
})

test('test gte', () => {
  let json = {
    '1': {
      'samples': '1'
    },
  }

  let inputString = 'samples gte 1'
  let tknFieldMap = {
    'samples': 'samples',
    'Type': 'samples',
    'name': 'samples',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern:frxt.configs.PunctuationPattern,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(1)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('samples')

  let filters = valid.map(({filter})=>filter)

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(1)
  expect(res[0]).toBe('1')
})

test('test lte', () => {
  let json = {
    '1': {
      'samples': '0'
    },
  }

  let inputString = 'samples lte 1'
  let tknFieldMap = {
    'samples': 'samples',
    'Type': 'samples',
    'name': 'samples',
  }

  let [valid, error] = frxt.extractFiltersFromText({
    input:inputString,
    tknFieldMap:tknFieldMap,
    tknFunctionMap:frxt.configs.defaultCNFFunctionMap,
    tknConditionalMap:frxt.configs.defaultCNFConditionalMap,
    fieldTokens:undefined,
    functionTokens:undefined,
    conditionalTokens:undefined,
    scrubInputPattern:frxt.configs.PunctuationPattern,
    cnfLogicConfig:frxt.configs.defaultCNFLogicConfig,
    cnfFunctionsConfig:frxt.configs.defaultCNFFunctionConfig,
    cnfConditionalConfig:frxt.configs.defaultCNFConditionalConfig,
    debug:false
  })
  expect(valid.length).toBe(1)
  expect(error.length).toBe(0)
  expect(valid[0].filter.field).toBe('samples')

  let filters = valid.map(({filter})=>filter)

  let res = frxt.conjunctiveNormalFormFilter(
    json,
    filters,
    frxt.configs.defaultCNFLogicConfig,
    frxt.configs.defaultCNFFunctionConfig,
    frxt.configs.defaultCNFConditionalConfig,
    {}
  )
  expect(res.length).toBe(1)
  expect(res[0]).toBe('1')
})