import {
  Filter,
  Filters,
  Field, Fields, JSONSQL, RecordIds, RecordId,
  CNFLogicConfig,
  CNFFunctionConfig,
  CNFConditionalConfig,
  TokenMapConfig,
  ExtractorConfig, ExtractorFunction,
  GenericConfig
} from './types'

import {
  identity
} from './utils'

import {jsonFields} from './jsonsql'

/**
 * @function groupByConjunctiveNormalForm
 * Group an array of filters such that each sub-array starts with a logical "and"
 * and all other filters are logical "or".
 * @param {Filters} filters - a list of filter objects
 * @returns {Filters[]} - lists of lists with each object in the inner
 * list being a filter.
 */
export const groupByConjunctiveNormalForm =
function(filters:Filters): Filters[] {
  let grouped = [];

  // for each filter
  for (let i = 0; i < filters.length; i++) {
    let logic = filters[i].logic;

    // logical "and" marks the beginning of a new sublist
    if (logic === "and") {
      grouped.push([filters[i]]);
      continue
    } else {
      // dealing with a logical "or", will be added to latest grouping
      let list = grouped[grouped.length - 1];

      // somehow, no logical "and" anywhere
      if (typeof list === "undefined") {
        // coerce logical "or" to logical "and"
        filters[i].logic = "and";
        grouped.push([filters[i]]);
      } else {
        grouped[grouped.length - 1].push(filters[i]);
      }
    }
  }
  return grouped as Filters[];
}

/**
 * @function getFilterAttributeFromConfig
 * given a Filter looks up the corresponding function as specificed
 * by prop in config
 * @param {Filter} filter - a list of filter objects
 * @param {string} prop - a list of filter objects
 * @param {CNFFunctionConfig|CNFConditionalConfig} config
 * @returns {Function}
 */
export const getFilterAttributeFromConfig =
function(
  filter:Filter, prop:string,
  config:CNFFunctionConfig|CNFConditionalConfig
) {
  // lookup default way of transforming funtions / conditionals as specified in
  // the passed `config`.
  let attr = config[filter[prop]];
  let func = attr[prop];
  if (attr.exceptions && filter.field in attr.exceptions) {
    return attr.exceptions[filter.field]
  }
  return func
}

/**
 * @function getExtractorFromConfig
 * retrieves the extractor function of the provided field from the provided
 * extractor config
 * @param {Field} field - field for which the extractor is requested
 * @param {ExtractorConfig} config - the config of extractor functions
 * @returns {ExtractorFunction}
 */
export const getExtractorFromConfig =
function(field:Field, config:ExtractorConfig): ExtractorFunction {
  if (field in config) {
    return config[field]
  }
  return identity
}

/**
 * @function conjunctiveNormalFormFilter
 * Applies a list of filters to the provided data.
 * @param {JSONSQL} json - SQL-esque JSON data
 * @param {Filters} filters - list of filters to be applied
 * @param {CNFLogicConfig} configLogic - conjunctive normal form logic
 * configuration
 * @param {CNFFunctionConfig} configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @param {ExtractorConfig} configExtractors - the config of extractor functions
 * for the provided json data.
 * @returns {RecordIds} filtered - the ids of the records in the provided json
 * data that passes the fitlers.
 */
export const conjunctiveNormalFormFilter =
function(
  json:JSONSQL,
  filters:Filters=[],
  configLogic:CNFLogicConfig={},
  configFunctions:CNFFunctionConfig={},
  configConditionals:CNFConditionalConfig={},
  configExtractors:ExtractorConfig={}
): RecordIds {
  let conjunctiveNormalForm = groupByConjunctiveNormalForm(filters);
  let ids = Object.keys(json);

  let filtered = ids.filter(id => {
    // extract current record
    let record = json[id];
    // calculate the truthiness of all logical "and" statements
    let and = conjunctiveNormalForm.map(groupedOrs => {
      // calculate the truthiness of all logical "or" statements
      let or = groupedOrs.some(filter => {

        // extract the function to apply
        let transform = getFilterAttributeFromConfig(filter, 'function', configFunctions)

        // extract the conditional to test
        let compare = getFilterAttributeFromConfig(filter, 'conditional', configConditionals)

        // get the current record's field to test against
        let extractor = getExtractorFromConfig(filter.field, configExtractors)
        let prop      = extractor(id, filter.field, record);

        // apply the transform to the field
        let value     = transform(prop);

        // get the truthiness of comparison
        let result    =  compare(value, filter.input);
        return result
      });
      return or;
    });
    return and.every(e => e === true);
  });
  return filtered;
}


/**
 * @function giRegexOnFields
 * global search across all fields for raw text
 * @param {string} text - raw text to try and match.
 * @param {JSONSQL} json - SQL-esque JSON data.
 * @param {RecordIds} ids - a subset of record ids from the provided json data
 * to search.
 * @param {Fields} fields - a subset of fields which occur in any given record
 * in which to search.
 * @param {ExtractorConfig} configExtractors - the config of extractor functions
 * for the provided json data. These values will be used when matching.
 * @returns {RecordIds} filtered - the ids of the records in the provided json
 * data that match the provided text when the extractor functions are applied.
 */
export const giRegexOnFields =
function (
  text:string,
  json:JSONSQL,
  ids:RecordIds=undefined,
  fields:Fields=undefined,
  configExtractors:ExtractorConfig={}
): RecordIds {

  let reg = new RegExp(text, "gi")
  let matches = [] as RecordIds;

  let idsToUse = (ids === undefined) ? Object.keys(json) : ids
  let fieldsToUse = (fields === undefined) ? jsonFields(json) : fields

  // nothing will match everything
  if (text === '') return idsToUse

  idsToUse.map(id => {
    let record = json[id];
    let fieldJoin = fieldsToUse.map(field => {
      let extractor = getExtractorFromConfig(field, configExtractors)
      return extractor(id, field, record);
    }).join('');

    let match = fieldJoin.match(reg);

    if (!(match == null || match.join('') == '')) {
      matches.push(id);
    }
  });

  return matches;
}

export default {
  groupByConjunctiveNormalForm,
  getFilterAttributeFromConfig,
  getExtractorFromConfig,
  conjunctiveNormalFormFilter,
  giRegexOnFields
}
