import {
  TokenMapConfig, Logic, Filter,
  FilterFromTextFnDef, FilterFromTextParams,
  FiltersFromTextFnDef, FiltersFromTextParams,
  CNFLogicConfig, CNFFunctionConfig, CNFConditionalConfig,
  ExtractedFilters
} from './types'

import {
  isLogic, isFilter, isValidFilter,
} from './utils'

import {PunctuationPattern} from './defaults'

/**
 * @function scrubPunctuation
 * removes invalid characters from a string
 * @param {string} text - the text to process.
 * @param {string|RegExp} pattern - a regex pattern of characters to remove.
 * @returns {string} - the text with matches to the provided pattern removed.
 */
export const scrubPunctuation =
function(text:string, pattern:string|RegExp = PunctuationPattern): string {
  return text.replace(pattern, "$1");
}

/**
 * @function tokensFromMap
 * returns the tokens from map
 * @param {TokenMapConfig} map - an object of {token: lookup} pairs
 * @returns {string[]} a list of unique tokens
 */
export const tokensFromMap =
function(map:TokenMapConfig={}): string[] {
  let keys = Object.keys(map)
  let values = Object.values(map)
  return [...new Set([...keys, ...values])];
}

/**
 * @function sortTokens
 * Sorts tokens in the order in which to apply them
 * @param {string[]} tokens - a list of tokens to try and match against.
 * @returns {string[]} tokens as sorted
 */
export const sortTokens =
function(tokens:string[]=[]): string[] {
  // copy array with .concat() to not affect order of passed item
  let tkns = tokens.concat()
  // sort tokens in descending length
  tkns.sort(function(a, b) {
    return b.length - a.length;
  });
  return tkns;
}

/**
 * @function extractTokensFromMap
 * Extracts the tokens from the provided mapping, sorted
 * @param {TokenMapConfig[]} map - the mapping of tokens.
 * @returns {string[]} tokens as sorted
 */
export const extractTokensFromMap =
function(map:TokenMapConfig={}): string[] {
  return sortTokens(tokensFromMap(map))
}


/**
 * @function doTokensSetMatch
 * given two token sets, returns whether or not they are identical
 * @param {string[]} tokenSet1 - a list of token parts
 * @param {string[]} tokenSet2 - a list of token parts
 * @returns {boolean} whether or not they match
 */
export const doTokensSetMatch =
function(tokenSet1:string[], tokenSet2:string[]): boolean {
  if (tokenSet1.length !== tokenSet2.length) return false;
  for (let i = tokenSet1.length; i--; ) {
    if (tokenSet1[i] !== tokenSet2[i]) return false;
  }
  return true;
}

/**
 * @function searchTokensForValues
 * searchs tokens for matching values
 * @description notably this function handles multi-token matching
 * @param {string[]} tokens - an array of input tokens of which to search for
 * tokens matching in values
 * @param {string[]} values - an array of value tokens from which to search
 * if an input token matches
 * @returns {[boolean, string, number, number]} a list of useful indicators
 * about the provided query:
 * [0]: {boolean}, whether or not a token in tokens matched one of values
 * [1]: {string}, the value that matched
 * [2]: {number}, the index of the value that matched
 * [3]: {number}, the index of the token that matched
 */
export const searchTokensForValues =
function (tokens:string[], values:string[]): [
  boolean, string, number, number
] {
  let valueIndex = 0;
  let tokenIndex = 0;
  let currentValue;
  let tokenizedValue;
  let tokensAreEqual = false;
  let consecutiveTokens;

  // for each value
  for (valueIndex = 0; valueIndex < values.length; valueIndex++) {
    // current value to match in tokens
    currentValue = values[valueIndex].toLowerCase();

    // "tokenize" current, in case of multiples word are in the value
    tokenizedValue = currentValue.split(' ');

    // look at n consecutive tokens where n is the number of tokenized values
    if (tokenizedValue.length > tokens.length) continue; // too long

    for (tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
      consecutiveTokens = tokens.slice(tokenIndex, tokenIndex + tokenizedValue.length);
      tokensAreEqual = doTokensSetMatch(tokenizedValue, consecutiveTokens);
      if (tokensAreEqual) break; // early exit
    }
    if (tokensAreEqual) break; // early exit
  }

  return [tokensAreEqual, values[valueIndex], valueIndex, tokenIndex];
}

/**
 * @function 
 * given an input string splits it by spaces, skipping regions with double quotes
 * @param {string} text - an input string
 * @returns {string[]} - A list of strings, where each string is a word
 * 
 * From: https://stackoverflow.com/a/46946420
 * 
 * Example: splitTextIntoWords('This is a list of words "that should\\" not be escaped"')
 * Returns: ['This', 'is', 'a', 'list', 'of', 'words', 'that should" not be escaped']
 */
const splitTextIntoWords =
function (text: string): string[] {
  var out = text.match(/\\?.|^$/g).reduce((p, c) => {
        if(c === '"'){
            p.quote ^= 1;
        }else if(!p.quote && c === ' '){
            p.a.push('');
        }else{
            p.a[p.a.length-1] += c.replace(/\\(.)/,"$1");
        }
        return  p;
    }, {a: [''], quote: 0}).a;
  return out;
}

/**
 * @function findLogicalStatements
 * given an input string attempts to parse it into logical statements (e.g.
 * and / or)
 * @param {string} text - an input string
 * @returns {number[]} - lists indices when the text is split by spaces of where
 * the logical statements begins
 */
export const findLogicalStatements =
function (text:string): number[] {
  let words = splitTextIntoWords(text);
  let statementIndicies = []
  for (let i = 0; i < words.length; i++) {
    let word = words[i]
    let isAnd = word.localeCompare('and', undefined, { sensitivity: 'base' })
    let isOr = word.localeCompare('or', undefined, { sensitivity: 'base' })

    // handel things like "greater than or equal to"
    let isEqual = 0
    if (i < words.length -1) {
      isEqual = words[i+1].localeCompare('equal', undefined, { sensitivity: 'base' })
    }

    if (isAnd === 0 || (isOr === 0 && isEqual !== 0)) {
      statementIndicies.push(i)
    }
  }
  return statementIndicies
}


/**
 * @function parseTextIntoStatements
 * splits the provided text into substrings each of which contains a logical
 * filter.
 * @param {string} text - an input string
 * @returns {string[]} - substrings each of which contains a logical filter
 */
export const parseTextIntoStatements =
function(text:string): string[] {
  // where each logical statement starts
  let statementStarts = findLogicalStatements(text)
  // no statements found, assume full text is statement with implied "and"
  if (statementStarts.length === 0) return [text]

  let statements = []
  let words = splitTextIntoWords(text)

  // slice text into statements
  statements.push(words.slice(0, statementStarts[0]).join(' '))
  for (let i = 0; i < statementStarts.length; i++) {
    let j = statementStarts[i]
    if (i === statementStarts.length-1) {
      statements.push(words.slice(j).join(' '))
    }
    else  {
      statements.push(words.slice(j, statementStarts[i+1]).join(' '))
    }
  }
  if (statements[0] === '') return statements.slice(1)
  return statements
}


/**
 * @function extractFilterFromText
 * attempts to extract a logical filter from the provided text and configs.
 * @param {Object} args - how to configure the function
 * @param {string} args.input - an input string from which to try and find a logical
 * filter.
 * @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields
 * @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions
 * @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals
 * @param {string[]} args.fieldTokens - the field tokens in order of which to search
 * @param {string[]} args.functionTokens - the function tokens in order of which to search
 * @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search
 * @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.
 * @param {boolean} args.debug - whether or not to debug to console.
 * @returns {Filter} - the extracted filter. if field and / or input is undefined
 * the extraction failed. By default logic, function, and conditional have the following
 * default values (and, identity, eq)
 */
export const extractFilterFromText:FilterFromTextFnDef =
function({
  /** @param {string} args.input - an input string from which to try and find a logical filter. */
  input,
  /** @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields */
  tknFieldMap = {},
  /** @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions */
  tknFunctionMap = {},
  /** @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals */
  tknConditionalMap = {},
  /** @param {string[]} args.fieldTokens - the field tokens in order of which to search */
  fieldTokens=undefined,
  /** @param {string[]} args.functionTokens - the function tokens in order of which to search */
  functionTokens=undefined,
  /** @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search */
  conditionalTokens=undefined,
  /** @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove. */
  scrubInputPattern = PunctuationPattern,
  /** @param {boolean} args.debug - whether or not to debug to console. */
  debug = false
}: FilterFromTextParams) : Filter {
  // STEP 0.1: initalize filter object to be returned
  let filter: Filter = {
    logic: 'and',
    function: 'identity',
    field: undefined,
    conditional: 'eq',
    input: undefined
  }


  // STEP 0.2: get tokens to match and ensure they are in descending order
  if (fieldTokens === undefined) fieldTokens = extractTokensFromMap(tknFieldMap)
  else fieldTokens = sortTokens(fieldTokens)
  if (functionTokens === undefined) functionTokens = extractTokensFromMap(tknFunctionMap)
  else functionTokens = sortTokens(functionTokens)
  if (conditionalTokens === undefined) conditionalTokens = extractTokensFromMap(tknConditionalMap)
  else conditionalTokens = sortTokens(conditionalTokens)

  if (debug) console.debug(fieldTokens, functionTokens, conditionalTokens)

  // STEP 1: preprocess text for later token matching
  // remove leading and trailing whitespace
  let text = `${input}`.trim();

  // remove all "invalid" punctuation
  text = scrubPunctuation(text, scrubInputPattern);
  if (debug) console.debug(`input post scrub:\t${text}`)

  // "tokenize"
  let textTokensUntouched = text.split(' ');

  // make lowercase to standardize comparisons
  let textTokens = textTokensUntouched.map(x => x.toLowerCase());


  // STEP 2: extract logic from input
  // ASSUMPTION: if logic is specified, it will be the first word.
  let firstWord = textTokens[0].toLowerCase();
  switch (isLogic(firstWord)) {
    case true:
      filter.logic = firstWord as Logic
      textTokens.splice(0, 1);
      textTokensUntouched.splice(0, 1);
    default:
      break;
  }

  // STEP 3: extract function from input
  // ASSUMPTION: default function is the identity function
  let [
    tokensAreEqual, // did we find a token
    currentValue,   // what token did we find
    valueIndex,     // index of token from passed values (arg2)
    tokenIndex      // index of token from passed tokens (arg1)
  ] = searchTokensForValues(textTokens, functionTokens);
  if (tokensAreEqual) {
    filter.function = tknFunctionMap[currentValue];
  }

  // STEP 4: extract conditional from input
  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, conditionalTokens);
  if (tokensAreEqual) {
    filter.conditional = tknConditionalMap[currentValue];
  }

  // everything after the conditional is "input", so need to keep track of index
  let conditionalIndex = tokenIndex;
  let conditionalValue = ((currentValue === undefined) ? '' : currentValue);


  // STEP 5: extract field on which the filter should be applied
  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, fieldTokens);
  if (tokensAreEqual) {
    filter.field = tknFieldMap[currentValue];
  }

  // STEP 6: return everything after the conditional
  // everything after the conditional is "input"
  filter.input = textTokensUntouched.slice(
    conditionalIndex + conditionalValue.split(' ').length,
    textTokens.length
  ).join(' ');

  return filter
}


/**
 * @function extractFilterFromText
 * attempts to extract multiple logical filters from the provided text and configs.
 * @param {Object} args - how to configure the function
 * @param {string} args.input - an input string from which to try and find a logical
 * filter.
 * @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields
 * @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions
 * @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals
 * @param {string[]} args.fieldTokens - the field tokens in order of which to search
 * @param {string[]} args.functionTokens - the function tokens in order of which to search
 * @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search
 * @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.
 * @param {CNFLogicConfig} args.configLogic - conjunctive normal form logic
 * configuration.
 * @param {CNFFunctionConfig} args.configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} args.configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @param {boolean} args.debug - whether or not to debug to console.
 * @returns {ExtractedFilters[]} - the valid filters ([0]) which were extracted and
 * the invalid filters ([1]) which were extracted.
 * @returns {Filter} - ExtractedFilters[].filter - the filter object
 * @returns {string} - ExtractedFilters[].text - the text which was processed to produce
 * the filter
 */
export const extractFiltersFromText: FiltersFromTextFnDef =
function({
  /** @param {string} args.input - an input string from which to try and find a logical filter.*/
  input,
  /** @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields*/
  tknFieldMap={},
  /** @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions*/
  tknFunctionMap={},
  /** @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals*/
  tknConditionalMap={},
  /** @param {string[]} args.fieldTokens - the field tokens in order of which to search*/
  fieldTokens=undefined,
  /** @param {string[]} args.functionTokens - the function tokens in order of which to search*/
  functionTokens=undefined,
  /** @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search*/
  conditionalTokens=undefined,
  /** @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.*/
  scrubInputPattern=PunctuationPattern,
  /** @param {CNFLogicConfig} args.configLogic - conjunctive normal form logic configuration.*/
  cnfLogicConfig={},
  /** @param {CNFFunctionConfig} args.configFunctions - conjunctive normal form function configuration, i.e. what functions are avaliable to be applied.*/
  cnfFunctionsConfig={},
  /** @param {CNFConditionalConfig} args.configConditionals - conjunctive normal form conditional configuration, i.e. what conditionals are avaliable to be applied.*/
  cnfConditionalConfig={},
  /** @param {boolean} args.debug - whether or not to debug to console.*/
  debug=false,
}:FiltersFromTextParams):[ExtractedFilters, ExtractedFilters] {
  if (fieldTokens === undefined) fieldTokens = extractTokensFromMap(tknFieldMap)
  else fieldTokens = sortTokens(fieldTokens)
  if (functionTokens === undefined) functionTokens = extractTokensFromMap(tknFunctionMap)
  else functionTokens = sortTokens(functionTokens)
  if (conditionalTokens === undefined) conditionalTokens = extractTokensFromMap(tknConditionalMap)
  else conditionalTokens = sortTokens(conditionalTokens)

  // break text into chunks of logical statments
  let statements = parseTextIntoStatements(input)
  let isValid = [], isError = []

  statements.forEach((statement)=>{
    let filter = extractFilterFromText({
      input:statement,
      tknFieldMap,
      tknFunctionMap,
      tknConditionalMap,
      fieldTokens,
      functionTokens,
      conditionalTokens,
      scrubInputPattern,
      debug
    })

    let hasProvidedValidFilter = isValidFilter(
      filter,
      Object.values(tknFieldMap),
      cnfLogicConfig as CNFLogicConfig,
      cnfFunctionsConfig as CNFFunctionConfig,
      cnfConditionalConfig as CNFConditionalConfig,
    )

    if (hasProvidedValidFilter) {
      isValid.push({filter, text:statement})
    } else {
      isError.push({filter, text:statement})
    }
  })
  return [isValid, isError]
}


export default {
  scrubPunctuation,
  tokensFromMap,
  sortTokens,
  extractTokensFromMap,
  doTokensSetMatch,
  searchTokensForValues,
  findLogicalStatements,
  parseTextIntoStatements,
  extractFilterFromText,
  extractFiltersFromText,
}
