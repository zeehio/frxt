import {
  ExtractorConfig, SortFunctionDef,
  SortSpecifications, RecordIds, JSONSQL,
  SortSpecification, Fields,
} from './types'
import {identity, isRecordSortable} from './utils'

/**
 * @function sortNumbers
 * Comparison function for numerical values.
 * By default JavaScript does _not_ sort numbers numerically.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export const sortNumbers:SortFunctionDef =
function(a:number, b:number, isAscending:boolean): boolean | number {
  return isAscending
    ? a - b
    : b - a
}


/**
 * @function sortStrings
 * Comparison function for string values.
 * Use of the method localeCompare with the following options specified:
 * - lang='en'
 * - sensitivity='base'
 * - ignorePunctuation=true
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export const sortStrings:SortFunctionDef =
function(a:string, b:string, isAscending:boolean): boolean | number {
  const lang = 'en'
  const opts = { sensitivity: 'base', ignorePunctuation: true }
  return isAscending
    ? a.localeCompare(b, lang, opts)
    : b.localeCompare(a, lang, opts)
}

/**
 * @function sortAtomic
 * A simple, dynamic sorting function. If both values are are string, compares
 * strings; if both numeric, compares magnitude, else returns none.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export const sortAtomic:SortFunctionDef =
function(a:any, b:any, isAscending:boolean): boolean | number {
  if (typeof a == 'string' && typeof b == 'string') {
    return sortStrings(a, b, isAscending)
  }
  else if (typeof a == 'number' && typeof b == 'number') {
    return sortNumbers(a, b, isAscending)
  }
  else {
    // return numberSort(a, b, isAscending)
    return  //-Infinity
  }
}



/**
 * @function sortableRecords
 * Determines which records can be sorted (has sortable values)
 * @param {JSONSQL} json - an SQL-like object
 * @param {RecordIds} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {Fields} fields - a list of fields which occur in each record
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 * @returns {[RecordIds, RecordIds]} the ids of the sortable records and those which
 * can not be sorted
 */
export function sortableRecords(
  json:JSONSQL,
  ids:RecordIds,
  fields:Fields,
  fieldSortByFunctions:ExtractorConfig={}
): [RecordIds, RecordIds] {

  let sortable = [], ignorable = [];

  ids.forEach(id => {
    let isSortable = isRecordSortable(json, id, fields, fieldSortByFunctions)
    if (isSortable) {
      // if all fields have a defined value, this record is sortable
      sortable.push(id)
    } else {
      // at least one field is missing a value, not sortable
      ignorable.push(id)
    }
  })
  return [sortable, ignorable]
}


/**
 * @function timsort
 * Peforms timsort on an SQL-like object
 * @param {JSONSQL} json - an SQL-like object
 * @param {SortSpecifications} sort - a list of sort specifications
 * @param {RecordIds} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 * @param {SortFunctionDef} defaultSortFunction - a function which handles unspecified the sorting
 * for unspecified fields in `fieldSortByFunctions`
 * @returns {RecordIds} sorted - the ids of the provided json data sorted according
 * to the sort specification of <sort>
 */
export function timsort(
  json:JSONSQL,
  sort:SortSpecifications,
  ids:RecordIds=[],
  fieldSortByFunctions:ExtractorConfig={},
  defaultSortFunction:SortFunctionDef=sortAtomic
): RecordIds {

  let use, sortable, ignorable;
  use = ids.length === 0 ? Object.keys(json) : [...ids]

  // no sort specified, return filtered in order that it was given
  if (sort.length === 0) {
    return use
  }

  // determine which fields are actually sortable
  [sortable, ignorable] = sortableRecords(json, use, sort.map(s=>s.field), fieldSortByFunctions)

  // setting up the comparison functions for the TimSort
  let comparisons = sort.map(({field, isAscending}) => {
    // each comparsion function takes the two record ids, extracts their values,
    // and then passes it to the defaultSort function
    return (id1, id2) => {
      const extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : identity
      let a = extractor(id1, field, json[id1])
      let b = extractor(id2, field, json[id2])
      return defaultSortFunction(a, b, isAscending)
    }
  })

  // the actual TimSort
  let sorted = sortable.sort((a, b) => {
    // while there is no difference, keep trying comparisons
    let difference
    for (let i = 0; i < comparisons.length; i++) {
      difference = comparisons[i](a, b);
      if (!difference) continue
      else break
    }
    return difference;
  });

  // return sorted and tack on the unsortable
  return sorted.concat(ignorable)
}


export const sortIndexFromSpecification =
function(sortSpecifications:SortSpecifications, field:string): number {
  for (let i = 0; i < sortSpecifications.length; i++) {
    if (sortSpecifications[i].field === field) {
      return i
    }
  }
  return -1
}

export const sortDirectionFromSpecification =
function(sortSpecifications:SortSpecifications, field:string): boolean | undefined {
  let i = sortIndexFromSpecification(sortSpecifications, field)
  if (i < 0) return undefined
  return sortSpecifications[i].isAscending
}


export const toggleSortSpecification =
function(sortSpecifications:SortSpecifications, field:string): SortSpecifications {
  let sortSpecs = sortSpecifications.concat()
  let sortSpec = {field: field, isAscending: true} as SortSpecification

  let i = sortIndexFromSpecification(sortSpecifications, field)
  let found = i > -1

  if (found) {
    let cur = sortSpecs[i]
    if (cur.isAscending) {
      sortSpec.isAscending = false
      sortSpecs[i] = sortSpec
    } else {
      sortSpecs.splice(i, 1)
    }
  }
  else {
    sortSpecs.push(sortSpec)
  }
  return sortSpecs
}


export default {
  sortNumbers,
  sortStrings,
  sortAtomic,
  sortableRecords,
  timsort,
  sortIndexFromSpecification,
  sortDirectionFromSpecification,
  toggleSortSpecification,
}
