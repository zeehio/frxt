export const PunctuationPattern = /(\w+(?:[".,-]\w+)*)|[.,()&$#![\]{}']+/g;

import {
  cnfLogicConfig as defaultCNFLogicConfig,
  cnfFunctionConfig as defaultCNFFunctionConfig,
  cnfConditionalConfig as defaultCNFConditionalConfig,
  tknFunctionMap as defaultCNFFunctionMap,
  tknConditionalMap as defaultCNFConditionalMap,
  fieldRenderFunctions as defaultFieldRenderFunctions,
  fieldFilterFunctions as defaultFieldFilterFunctions,
  fieldSortByFunctions as defaultFieldSortByFunctions,
} from './configs'

export {
  defaultCNFLogicConfig,
  defaultCNFFunctionConfig,
  defaultCNFConditionalConfig,
  defaultCNFFunctionMap,
  defaultCNFConditionalMap,
  defaultFieldRenderFunctions,
  defaultFieldFilterFunctions,
  defaultFieldSortByFunctions,
}

export default {
  PunctuationPattern,
  defaultCNFLogicConfig,
  defaultCNFFunctionConfig,
  defaultCNFConditionalConfig,
  defaultCNFFunctionMap,
  defaultCNFConditionalMap,
  defaultFieldRenderFunctions,
  defaultFieldFilterFunctions,
  defaultFieldSortByFunctions,
}
